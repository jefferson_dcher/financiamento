export default class FinanciamentoSaida {

    constructor(valorImovel, valorEntrada, valorFinanciado, parcela) {
        this._valorImovel = valorImovel;
        this._valorEntrada = valorEntrada;
        this._valorFinanciado = valorFinanciado;
        this._parcelas = [];
        this.addParcela(parcela);
    }

    get valorImovel() {
        return this_valorImovel;
    }

    set valorImovel(val) {
        this._valorImovel = val;
    }

    get valorEntrada() {
        return this._valorEntrada;
    }

    set valorEntrada(val) {
        this._valorEntrada = val;
    }

    get valorFinanciado() {
        return this._valorFinanciado;
    }

    set valorFinanciado(val) {
        this._valorFinanciado = val;
    }

    addParcela(parcela) {
        this._parcelas.push(parcela);
      }

    get parcelas() {
        return this._parcelas;
    }

}