export default class Parcela {

    _numeroParcela;
    _amortizacao;
    _juro;
    _valor;
    _saldoDevedor;

    /* constructor(numeroParcela, amortizacao, juro, valor, saldoDevedor) {
        this._numeroParcela = numeroParcela;
        this._amortizacao = amortizacao;
        this._juro = juro;
        this._valor = valor;
        this._saldoDevedor = saldoDevedor;
    } */

    constructor() { }

    get numeroParcela() {
        return this._numeroParcela;
    }

    set numeroParcela(val) {
        this._numeroParcela = val;
    }

    get amortizacao() {
        return this._amortizacao;
    }

    set amortizacao(val) {
        this._amortizacao = val;
    }

    get juro() {
        return this._juro;
    }

    set juro(val) {
        this._juro = val;
    }

    get valor() {
        return this._valor;
    }

    set valor(val) {
        this._valor = val;
    }

    get saldoDevedor() {
        return this._saldoDevedor;
    }

    set saldoDevedor(val) {
        this._saldoDevedor = val;
    }

}