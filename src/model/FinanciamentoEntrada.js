export default class FinanciamentoEntrada {

    constructor(valorImovel, percentualEntrada, taxaFinanciamento, quantidadeParcelas) {
        this._valorImovel = valorImovel;
        this._percentualEntrada = percentualEntrada;
        this._taxaFinanciamento = taxaFinanciamento;
        this._quantidadeParcelas = quantidadeParcelas;
    }

    get valorImovel() {
        return this._valorImovel;
    }

    set valorImovel(val) {
        this._valorImovel = val;
    }

    get percentualEntrada() {
        return this._percentualEntrada;
    }

    set percentualEntrada(val) {
        this._percentualEntrada = val;
    }

    get taxaFinanciamento() {
        return this._taxaFinanciamento;
    }
    set taxaFinanciamento(val) {
        this._taxaFinanciamento = val;
    }

    get quantidadeParcelas() {
        return this._quantidadeParcelas;
    }

    set quantidadeParcelas(val) {
        this._quantidadeParcelas = val;
    }

}