import React, { Component } from 'react';

import FinanciamentoEntrada from '../model/FinanciamentoEntrada';
import Parcela from '../model/Parcela';
import FinanciamentoSaida from '../model/FinanciamentoSaida';

type Props = {};
export default class Calculo extends Component<Props> {

    constructor(valorImovel, percentualEntrada, taxaFinanciamento, quantidadeParcelas) {
        super();
        financiamentoEntrada = new FinanciamentoEntrada(valorImovel, percentualEntrada, taxaFinanciamento, quantidadeParcelas);
    }


    get valorEntrada() {
        return financiamentoEntrada.valorImovel * (financiamentoEntrada.percentualEntrada / 100);
    }

    get taxaMensal() {
        return financiamentoEntrada.taxaFinanciamento / 12;
    }

    get valorFinanciado() {
        return financiamentoEntrada.valorImovel - this.valorEntrada;
    }

    get amortizacao() {
        return this.valorFinanciado / financiamentoEntrada.quantidadeParcelas;
    }

    gerarParcelas() {

    }

    gerarParcela(numeroParcela, amortizacao, juro, valor, saldoDevedor) {
        let parcela = new Parcela();

        
        
    }








}