import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';

import Financiamento from './screens/Financiamento';
import Resultado from './screens/Resultado';

Navigation.registerComponent(`financiamento`, () => Financiamento);
Navigation.registerComponent(`resulatado`, () => Resultado);

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            component: {
                id: 'home',
                name: 'financiamento'
            }
        }
    });
});