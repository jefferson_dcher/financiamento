import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Navigation } from 'react-native-navigation';

import Calculo from '../components/Calculo';

import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    Form,
    Item,
    Label,
    Input
} from 'native-base';

type Props = {};
export default class Financiamento extends Component<Props> {

    constructor() {
        super();
        this.state = {
            valorImovel: 0,
            percentualEntrada: 0,
            taxaFinanciamento: 0,
            quantidadeParcelas: 0
        }
    }

    calcula() {
        let calculo = new Calculo(this.state.valorImovel, this.state.percentualEntrada, this.state.taxaFinanciamento, this.state.quantidadeParcelas);
        this.mostraResultado(calculo);
    }

    mostraResultado(obj) {
        Navigation.push('homeStack', {
            component: {
                name: 'resulatado',
                passProps: {
                    calback: this.state,
                    calculo: obj
                },
                options: {
                    topBar: {
                        title: {
                            text: 'Resultado'
                        }
                    }
                }
            }
        });
    }

    render() {

        return (
            <Container>
                <Content>
                    <Form>
                        <Item floatingLabel>
                            <Label>Valor do Imóvel</Label>
                            <Input onChangeText={(valorImovel) => this.setState({ valorImovel })} keyboardType="decimal-pad" />
                        </Item>
                        <Item floatingLabel last>
                            <Label>Percentual de Entrada</Label>
                            <Input onChangeText={(percentualEntrada) => this.setState({ percentualEntrada })} keyboardType="decimal-pad" />
                        </Item>
                        <Item floatingLabel last>
                            <Label>Taxa de Financiamento</Label>
                            <Input onChangeText={(taxaFinanciamento) => this.setState({ taxaFinanciamento })} keyboardType="decimal-pad" />
                        </Item>
                        <Item floatingLabel last>
                            <Label>QTD. de Parcelas</Label>
                            <Input onChangeText={(quantidadeParcelas) => this.setState({ quantidadeParcelas })} keyboardType="decimal-pad" />
                        </Item>
                        <View style={styles.contemBotao}>
                            <Button onPress={() => this.calcula()} style={styles.botao} full success>
                                <Text>Calcular</Text>
                            </Button>
                        </View>

                    </Form>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#00685b'
    },
    botao: {
        backgroundColor: '#00685b'
    },
    contemBotao: {
        marginTop: 30,
        padding: 10
    }
});
