import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Text,
    Form,
    Item,
    Label,
    Input,
    Card,
    CardItem
} from 'native-base';

type Props = {};
export default class Resultado extends Component<Props> {

    constructor() {
        super();
        this.state = {
            valorEntrada: 0,
            taxaMensal: 0,
            valorFinanciado: 0,
            amortizacaoMensal: 0,

            juros: 0,
            valorDaParcela: 0,
            saldoDevedor: 0
        }
    }


    componentDidMount() {
        this.setState({
            valorEntrada: this.props.calculo.valorEntrada,
            taxaMensal: this.props.calculo.taxaMensal,
            valorFinanciado: this.props.calculo.valorFinanciado,
            amortizacaoMensal: this.props.calculo.amortizacao,

            /* valorEntrada: this.props.calback.valorImovel * (this.props.calback.percentualEntrada / 100),
            taxaMensal: this.props.calback.taxaFinanciamento / 12,
            valorFinanciado: this.props.calback.valorImovel - (this.props.calback.valorImovel * (this.props.calback.percentualEntrada / 100)),
            amortizacaoMensal: (this.props.calback.valorImovel - (this.props.calback.valorImovel * (this.props.calback.percentualEntrada / 100))) / this.props.calback.quantidadeParcelas,

            juros: (this.props.calback.valorImovel * (this.props.calback.percentualEntrada / 100)) * (this.props.calback.taxaFinanciamento / 12),
            valorDaParcela: ((this.props.calback.valorImovel * (this.props.calback.percentualEntrada / 100)) * (this.props.calback.taxaFinanciamento / 12)) + ((this.props.calback.valorImovel - (this.props.calback.valorImovel * (this.props.calback.percentualEntrada / 100))) / this.props.calback.quantidadeParcelas),
            saldoDevedor: this.props.calback.valorImovel * (this.props.calback.percentualEntrada / 100) */
        });
    }



    render() {
        /* var parcelas = [];
        for (var index = 1; index <= this.props.calback.quantidadeParcelas; index++) {
            parcelas.push(
                <CardItem key={index}>
                    <View style={styles.linhasView}>
                        <View style={styles.linhaView}>
                            <Text>{index}</Text>
                        </View>
                        <View style={styles.linhaView}>
                            <Text>100,00</Text>
                        </View>
                        <View style={styles.linhaView}>
                            <Text>100,00</Text>
                        </View>
                        <View style={styles.linhaView}>
                            <Text>{this.state.saldoDevedor}</Text>
                        </View>
                    </View>
                </CardItem>
            )
        } */
        return (
            <Container>
                <Content>

                    <View style={styles.card}>
                        <Card>
                            <CardItem>
                                <Text>Valores de Entrada</Text>
                            </CardItem>
                            <CardItem style={styles.cardItem} cardBody>
                                <Text>Valor do Imóvel: {this.props.calback.valorImovel}</Text>
                            </CardItem>
                            <CardItem style={styles.cardItem} cardBody>
                                <Text>Percentual de entrada: {this.props.calback.percentualEntrada}</Text>
                            </CardItem>
                            <CardItem style={styles.cardItem} cardBody>
                                <Text>Taxa de financiamento: {this.props.calback.taxaFinanciamento}</Text>
                            </CardItem>
                            <CardItem style={styles.cardItem} cardBody>
                                <Text>QTD. de Parcelas: {this.props.calback.quantidadeParcelas}</Text>
                            </CardItem>
                        </Card>
                    </View>

                    <View style={styles.card}>
                        <Card>
                            <CardItem>
                                <Text>Cálculos do Financiamento</Text>
                            </CardItem>
                            <CardItem style={styles.cardItem} cardBody>
                                <Text>Valor de Entrada: {this.state.valorEntrada}</Text>
                            </CardItem>
                            <CardItem style={styles.cardItem} cardBody>
                                <Text>Taxa Mensal: {this.state.taxaMensal}</Text>
                            </CardItem>
                            <CardItem style={styles.cardItem} cardBody>
                                <Text>Valor Financiado: {this.state.valorFinanciado}</Text>
                            </CardItem>
                            <CardItem style={styles.cardItem} cardBody>
                                <Text>Amortização Mensal: {this.state.amortizacaoMensal}</Text>
                            </CardItem>
                        </Card>
                    </View>

                    {/* <View style={styles.card}>
                        <Card>
                            <CardItem key={index}>
                                <View style={styles.linhasView}>
                                    <View style={styles.linhaView}>
                                        <Text>Parcela</Text>
                                    </View>
                                    <View style={styles.linhaView}>
                                        <Text>Juros</Text>
                                    </View>
                                    <View style={styles.linhaView}>
                                        <Text>Valor</Text>
                                    </View>
                                    <View style={styles.linhaView}>
                                        <Text>Saldo Devedor</Text>
                                    </View>
                                </View>
                            </CardItem>
                            {parcelas}
                        </Card>
                    </View> */}

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        marginTop: 20,
        padding: 10
    },
    cardItem:{
        paddingLeft: 20,
        paddingRight: 20
    },
    linhasView: {
        flex: 1,
        flexDirection: 'row'
    },
    linhaView: {
        flex: 1
    }
});
