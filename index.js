/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */
import { Navigation } from "react-native-navigation";
import Financiamento from './src/screens/Financiamento';
import Resultado from './src/screens/Resultado';

Navigation.registerComponent(`financiamento`, () => Financiamento);
Navigation.registerComponent(`resulatado`, () => Resultado);

Navigation.setDefaultOptions({
    topBar: {
      background: {
          color: 'teal'
      },
      title: {
          color: '#FFF'
      },
      backButton: {
          color: '#FFF'
      }
    }
  });

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            stack: {
                id: 'homeStack',
                children: [
                    {
                        component: {
                            id: 'home',
                            name: 'financiamento',
                            options: {
                                topBar: {
                                  title: {
                                    text: 'Financiamento'
                                  }
                                }
                              }
                        }
                    }
                ]
            }
        }
    });
});
